const nav = document.querySelector("#nav-port")

if(window.scrollY >= 10) {
	nav.classList.add("bg-primary")
}

window.onscroll = () => {
	if(this.scrollY <=10) {
		nav.classList.remove("bg-primary")
		nav.classList.add("bg-transparent")
	} else {
		nav.classList.remove("bg-transparent")
		nav.classList.add("bg-primary")
	}
}